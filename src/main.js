import Vue from 'vue'
import App from './App.vue'
import router from './router'

import VueCodeHighlight from 'vue-code-highlight'

import 'prism-es6/components/prism-markup-templating'
import 'prism-es6/components/prism-scss'
import 'vue-code-highlight/themes/duotone-sea.css'

Vue.use(VueCodeHighlight)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
