import Vue from 'vue'
import VueRouter from 'vue-router'
import ButtonPage from '../views/ButtonPage'
import CardPage from '../views/CardPage'
import HomePage from '../views/HomePage'
import FormPage from '../views/FormPage'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: HomePage
  },
  {
    path: '/button',
    name: 'ButtonPage',
    component: ButtonPage
  },
  {
    path: '/card',
    name: 'CardPage',
    component: CardPage
  },
  {
    path: '/form',
    name: 'FormPage',
    component: FormPage
  }
  // {
  //   path: '/about',
  //   name: 'About',
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'active',
  routes
})

export default router
